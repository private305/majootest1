package main

import (
	"log"
	"net/http"
	"propose/configs"
	"propose/services"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	err := configs.OpenConnection()
	if err != nil {
		log.Println(err)
		panic("Failed connect DB")
	}
	defer configs.DB.Close()

	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.GET("/login", services.Login)
	api := e.Group("/api")
	api.Use(middleware.JWT([]byte("1234567890")))
	api.GET("/addUser", services.AddUser)
	api.GET("/getUsers", services.GetUsers)
	api.GET("/dropUser", services.DropUser)
	api.GET("/updateUser", services.UpdateUser)

	api.GET("/addOrg", services.AddOrg)
	api.GET("/getOrg", services.GetOrgs)

	e.Logger.Fatal(e.Start(":10000"))
}
