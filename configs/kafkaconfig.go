package configs

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func kafkaConn() {
	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "host1:9092,host2:9092",
		"client.id":         socket.gethostname(),
		"acks":              "all"})
}
