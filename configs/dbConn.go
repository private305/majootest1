package configs

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/lib/pq"
)

var (
	DB       *sql.DB
	DBUser   = "postgres"
	DBPass   = "1234"
	DBName   = "propose"
	DBHost   = "localhost"
	DBPort   = "5432"
	SSLMode  = "disable"
	DBDriver = "postgres"
)

func OpenConnection() error {
	var err error
	DB, err = setupConnection()

	return err
}

// setupConnection adalah
func setupConnection() (*sql.DB, error) {
	var connection = fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=%s",
		DBUser, DBPass, DBName, DBHost, DBPort, SSLMode)
	fmt.Println("Connection Info:", DBDriver, connection)

	db, err := sql.Open(DBDriver, connection)
	if err != nil {
		return db, errors.New("Connection closed: Failed Connect Database")
	}

	return db, nil
}
