package services

import (
	"crypto/md5"
	"database/sql"
	"encoding/hex"
	"fmt"
	"io"
	"net/http"
	"propose/models"
	"propose/repositories"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

// type jwtCustomClaims struct {
// 	UserID     int    `json:"userId"`
// 	Username   string `json:"username"`
// 	Superadmin bool   `json:"superadmin"`
// 	jwt.MapClaims
// }

func Login(c echo.Context) error {
	var (
		request models.ReqGetUsers
	)
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(err)
		return c.String(http.StatusOK, "Validation Failed!")
	}
	if request.NickName == "" {
		return c.String(http.StatusOK, "Validation Failed! :: NickName cannot be null")
	}
	if request.Username == "" {
		return c.String(http.StatusOK, "Validation Failed! :: Username cannot be null")
	}
	if request.Password == "" {
		return c.String(http.StatusOK, "Validation Failed! :: Password cannot be null")
	}
	//generate password md5
	hash := md5.New()
	io.WriteString(hash, request.Password) // append into the hash
	request.Password = hex.EncodeToString(hash.Sum(nil))
	resp, err := repositories.GetUser(models.ReqGetUsers{
		Username: request.Username,
	})
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, "Not Found!")
	}
	if resp.Password != request.Password {
		fmt.Println("wrong username or passsword")
		return c.JSON(http.StatusOK, "wrong username or passsword!")
	}
	//send to kafka loggin success
	if resp.Role == "-1" {
		resp.Role = "superadmin"
	} else {
		resp.Role = "user"
	}
	//generate jwt
	token, err := JWTGenerator(strconv.Itoa(resp.Id), resp.Username, resp.Role)
	if err != nil {
		fmt.Println(err, "JWTGenerator")
		return c.JSON(http.StatusOK, "login failed!")
	}
	type responLogin struct {
		Token string              `json:"token"`
		Data  models.RespGetUsers `json:"data"`
	}
	result := models.RespGetUsers{
		Id:       resp.Id,
		NickName: resp.NickName,
		Address:  resp.Address,
		Age:      resp.Age,
		Role:     resp.Role,
		Password: "######",
		Username: resp.Username,
	}
	response := responLogin{
		Token: token,
		Data:  result,
	}
	return c.JSON(http.StatusOK, response)
} //ok
func AddUser(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	superadmin := claims["superadmin"].(string)
	if superadmin != "superadmin" {
		fmt.Println("not authorized")
		return c.JSON(http.StatusOK, "not authorized!")
	}
	var request models.ReqGetUsers
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, "Validation Failed!")
	}
	if request.NickName == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: NickName cannot be null")
	}
	if request.Username == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: Username cannot be null")
	}
	if request.Password == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: Password cannot be null")
	}
	resps, err := repositories.GetUsers(models.ReqGetUsers{
		Username: request.Username,
	})
	if err != nil {
		if err != sql.ErrNoRows {
			fmt.Println(err, "GetUser")
			return c.JSON(http.StatusOK, "Add User Failed!")
		}
	}
	if len(resps) != 0 {
		return c.JSON(http.StatusOK, "Username is exist!")
	}
	//generate password md5
	hash := md5.New()
	io.WriteString(hash, request.Password) // append into the hash
	request.Password = hex.EncodeToString(hash.Sum(nil))
	resp, err := repositories.AddUser(request)
	if err != nil {
		fmt.Println(err, "AddUser")
		return c.JSON(http.StatusOK, "Add User Failed!")
	}
	resp.Password = "######"
	return c.JSON(http.StatusOK, resp)
} //ok
func GetUsers(c echo.Context) error {
	var (
		request models.ReqGetUsers
		reqRepo models.ReqGetUsers
	)
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(":::", err)
		return c.String(http.StatusOK, "Validation Failed!")
	}
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userId := claims["userId"].(string)
	superadmin := claims["superadmin"].(string)
	Id, _ := strconv.Atoi(userId)
	if superadmin != "superadmin" {
		reqRepo.Id = Id
	} else {
		reqRepo = request
	}
	resp, err := repositories.GetUsers(reqRepo)
	if err != nil {
		fmt.Println(":::", err)
		return c.String(http.StatusOK, "Failed!")
	}
	return c.JSON(http.StatusOK, resp)
} //ok
func DropUser(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	superadmin := claims["superadmin"].(string)
	if superadmin != "superadmin" {
		fmt.Println("not authorized")
		return c.JSON(http.StatusOK, "not authorized!")
	}
	var request models.ReqGetUsers
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, "Validation Failed!")
	}
	if request.Id == 0 {
		return c.JSON(http.StatusOK, "Validation Failed! :: Id cannot be null")
	}
	// resp, err := repositories.GetUser(models.ReqGetUsers{
	// 	Id: request.Id,
	// })
	// if err != nil {
	// 	fmt.Println(err, "GetUser")
	// 	return c.JSON(http.StatusOK, "Drop Failed!")
	// }
	// if resp.Role != "-1" {
	// 	fmt.Println("Failed, call yout administrator")
	// 	return c.JSON(http.StatusOK, "Failed, call yout administrator!")
	// }
	status := repositories.DropUser(request.Id)
	if !status {
		return c.JSON(http.StatusOK, "Delete User Failed!")
	}
	return c.JSON(http.StatusOK, "Success!")
} //ok
func UpdateUser(c echo.Context) error {
	var request models.ReqGetUsers
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, "Validation Failed!")
	}
	if request.NickName == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: NickName cannot be null")
	}
	if request.Username == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: Username cannot be null")
	}
	if request.Password == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: Password cannot be null")
	}
	resps, err := repositories.GetUsers(models.ReqGetUsers{
		Id: request.Id,
	})
	if err != nil {
		fmt.Println(err, "GetUser")
		return c.JSON(http.StatusOK, "Update User Failed!")
	}
	if len(resps) == 0 {
		return c.JSON(http.StatusOK, "user not found!")
	}
	hash := md5.New()
	io.WriteString(hash, request.Password) // append into the hash
	request.Password = hex.EncodeToString(hash.Sum(nil))
	resp, err := repositories.UpdateUser(request)
	if err != nil {
		fmt.Println(err, "UpdateUser")
		return c.JSON(http.StatusOK, "Add User Failed!")
	}
	resp.Password = "######"
	return c.JSON(http.StatusOK, resp)
} //ok
func JWTGenerator(id string, username string, isAdmin string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["userId"] = id
	claims["username"] = username
	claims["superadmin"] = isAdmin
	claims["exp"] = time.Now().Add(time.Hour * 3).Unix()
	restoken, err := token.SignedString([]byte("1234567890"))
	if err != nil {
		return "", err
	}
	return restoken, nil
}
