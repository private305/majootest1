package services

import (
	"database/sql"
	"fmt"
	"net/http"
	"propose/models"
	"propose/repositories"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

func AddOrg(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	superadmin := claims["superadmin"].(string)
	if superadmin != "superadmin" {
		fmt.Println("not authorized")
		return c.JSON(http.StatusOK, "not authorized!")
	}
	var request models.ReqGetDatas
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusOK, "Validation Failed!")
	}
	if request.NickName == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: NickName cannot be null")
	}
	if request.UserId == 0 {
		return c.JSON(http.StatusOK, "Validation Failed! :: User Id cannot be null")
	}
	if request.PositionId == 0 {
		return c.JSON(http.StatusOK, "Validation Failed! :: Position Id cannot be null")
	}
	if request.PositionName == "" {
		return c.JSON(http.StatusOK, "Validation Failed! :: Position Name cannot be null")
	}
	resps, err := repositories.GetDatas(models.ReqGetDatas{
		UserId:     0,
		PositionId: 0,
	})
	if err != nil {
		if err != sql.ErrNoRows {
			fmt.Println(err, "GetUser")
			return c.JSON(http.StatusOK, "Add User Failed!")
		}
	}
	if len(resps) != 0 {
		return c.JSON(http.StatusOK, "Username is exist!")
	}

	resp, err := repositories.AddData(request)
	if err != nil {
		fmt.Println(err, "AddUser")
		return c.JSON(http.StatusOK, "Add User Failed!")
	}
	return c.JSON(http.StatusOK, resp)
} //ok
func GetOrgs(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	superadmin := claims["superadmin"].(string)
	if superadmin != "superadmin" {
		fmt.Println("not authorized")
		return c.JSON(http.StatusOK, "not authorized!")
	}
	var (
		request models.ReqGetDatas
	)
	err := c.Bind(&request)
	if err != nil {
		fmt.Println(":::", err)
		return c.String(http.StatusOK, "Validation Failed!")
	}
	resp, err := repositories.GetDatas(request)
	if err != nil {
		fmt.Println(":::", err)
		return c.String(http.StatusOK, "Failed!")
	}
	return c.JSON(http.StatusOK, resp)
} //ok
// func DropUser(c echo.Context) error {
// 	user := c.Get("user").(*jwt.Token)
// 	claims := user.Claims.(jwt.MapClaims)
// 	superadmin := claims["superadmin"].(string)
// 	if superadmin != "superadmin" {
// 		fmt.Println("not authorized")
// 		return c.JSON(http.StatusOK, "not authorized!")
// 	}
// 	var request models.ReqGetUsers
// 	err := c.Bind(&request)
// 	if err != nil {
// 		fmt.Println(err)
// 		return c.JSON(http.StatusOK, "Validation Failed!")
// 	}
// 	if request.Id == 0 {
// 		return c.JSON(http.StatusOK, "Validation Failed! :: Id cannot be null")
// 	}
// 	// resp, err := repositories.GetUser(models.ReqGetUsers{
// 	// 	Id: request.Id,
// 	// })
// 	// if err != nil {
// 	// 	fmt.Println(err, "GetUser")
// 	// 	return c.JSON(http.StatusOK, "Drop Failed!")
// 	// }
// 	// if resp.Role != "-1" {
// 	// 	fmt.Println("Failed, call yout administrator")
// 	// 	return c.JSON(http.StatusOK, "Failed, call yout administrator!")
// 	// }
// 	status := repositories.DropUser(request.Id)
// 	if !status {
// 		return c.JSON(http.StatusOK, "Delete User Failed!")
// 	}
// 	return c.JSON(http.StatusOK, "Success!")
// } //ok
// func UpdateUser(c echo.Context) error {
// 	var request models.ReqGetUsers
// 	err := c.Bind(&request)
// 	if err != nil {
// 		fmt.Println(err)
// 		return c.JSON(http.StatusOK, "Validation Failed!")
// 	}
// 	if request.NickName == "" {
// 		return c.JSON(http.StatusOK, "Validation Failed! :: NickName cannot be null")
// 	}
// 	if request.Username == "" {
// 		return c.JSON(http.StatusOK, "Validation Failed! :: Username cannot be null")
// 	}
// 	if request.Password == "" {
// 		return c.JSON(http.StatusOK, "Validation Failed! :: Password cannot be null")
// 	}
// 	resps, err := repositories.GetUsers(models.ReqGetUsers{
// 		Id: request.Id,
// 	})
// 	if err != nil {
// 		fmt.Println(err, "GetUser")
// 		return c.JSON(http.StatusOK, "Update User Failed!")
// 	}
// 	if len(resps) == 0 {
// 		return c.JSON(http.StatusOK, "user not found!")
// 	}
// 	hash := md5.New()
// 	io.WriteString(hash, request.Password) // append into the hash
// 	request.Password = hex.EncodeToString(hash.Sum(nil))
// 	resp, err := repositories.UpdateUser(request)
// 	if err != nil {
// 		fmt.Println(err, "UpdateUser")
// 		return c.JSON(http.StatusOK, "Add User Failed!")
// 	}
// 	resp.Password = "######"
// 	return c.JSON(http.StatusOK, resp)
// } //ok
