package repositories

import (
	"errors"
	"fmt"
	"propose/configs"
	"propose/models"
	"strconv"
)

// organization
const tb1 = `
user_id,
user_nickname,
position_id,
position_name
`

func GetDatas(req models.ReqGetDatas) (result []models.RespGetDatas, err error) {
	query := `select id,` + tb1 + ` from organizations where true `
	if req.Id != 0 {
		query += ` and id = '` + strconv.Itoa(req.Id) + `'`
	}
	if req.UserId != 0 {
		query += ` and user_id = '` + strconv.Itoa(req.UserId) + `'`
	}
	if req.PositionId != 0 {
		query += ` and position_id = '` + strconv.Itoa(req.PositionId) + `'`
	}
	if req.NickName != "" {
		query += ` and user_nickname = '` + req.NickName + `'`
	}
	if req.PositionName != "" {
		query += ` and position_name = '` + req.PositionName + `'`
	}
	if req.AscDesc != "" {
		query += `order by user_nickname ` + req.AscDesc
	} else {
		query += ` order by user_nickname asc `
	}

	rows, err := configs.DB.Query(query)
	if err != nil {
		return result, err
	}
	defer rows.Close()
	for rows.Next() {
		var val models.RespGetDatas
		err = rows.Scan(
			&val.Id,
			&val.UserId,
			&val.UserNickName,
			&val.PositionId,
			&val.PositionName,
		)
		if err != nil {
			return result, err
		}
		result = append(result, val)
	}
	if len(result) == 0 {
		return result, errors.New("Not Found")
	}
	return result, nil
}
func GetData(req models.ReqGetDatas) (result models.RespGetDatas, err error) {
	query := `select id,` + tb1 + ` from organizations where true `
	if req.Id != 0 {
		query += ` and id = '` + strconv.Itoa(req.Id) + `'`
	}
	if req.UserId != 0 {
		query += ` and user_id = '` + strconv.Itoa(req.UserId) + `'`
	}
	if req.PositionId != 0 {
		query += ` and position_id = '` + strconv.Itoa(req.PositionId) + `'`
	}
	if req.NickName != "" {
		query += ` and user_nickname = '` + req.NickName + `'`
	}
	if req.PositionName != "" {
		query += ` and position_name = '` + req.PositionName + `'`
	}
	if req.AscDesc != "" {
		query += `order by user_nickname ` + req.AscDesc
	} else {
		query += ` order by user_nickname asc `
	}
	err = configs.DB.QueryRow(query).Scan(
		&result.Id,
		&result.UserId,
		&result.UserNickName,
		&result.PositionId,
		&result.PositionName,
	)
	if err != nil {
		return result, err
	}
	return result, err
}
func AddData(req models.ReqGetDatas) (result models.RespGetDatas, err error) {
	query := `insert into organizations (` + tb1 + `) values ($1,$2,$3,$4) returning id,` + tb1
	err = configs.DB.QueryRow(query, req.UserId, req.NickName, req.PositionId, req.PositionName).Scan(
		&result.Id,
		&result.UserId,
		&result.UserNickName,
		&result.PositionId,
		&result.PositionName,
	)
	if err != nil {
		return result, err
	}
	return result, err
}
func DropData(id int) (status bool) {
	query := `delete from organizations where id=$1`
	_, err := configs.DB.Exec(query, id)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
func UpdateData(req models.ReqGetDatas) (result models.RespGetDatas, err error) {
	query := `update organizations set 
	user_id=$1,
	user_nickname=$2,
	position_id=$3,
	position_name=$4
	 where id=$5 returning id,` + tb
	err = configs.DB.QueryRow(query, req.UserId, req.NickName, req.PositionId, req.PositionName, req.Id).Scan(
		&result.Id,
		&result.UserId,
		&result.UserNickName,
		&result.PositionId,
		&result.PositionName,
	)
	if err != nil {
		return result, err
	}
	return result, nil
}
