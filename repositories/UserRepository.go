package repositories

import (
	"fmt"
	"propose/configs"
	"propose/models"
	"strconv"
)

// users
const tb = `
nickname,
age,
address,
role,
password,
username
`

var position = [][]string{
	{1: "Head"},
	{2: "Secretary"},
	{3: "Staff"},
}

func GetUsers(req models.ReqGetUsers) (result []models.RespGetUsers, err error) {
	query := `select id,` + tb + ` from users where true `
	if req.Id != 0 {
		query += ` and id = '` + strconv.Itoa(req.Id) + `'`
	}
	if req.Age != 0 {
		query += ` and age = '` + strconv.Itoa(req.Age) + `'`
	}
	if req.NickName != "" {
		query += ` and nickname = '` + req.NickName + `'`
	}
	if req.Username != "" {
		query += ` and username = '` + req.Username + `'`
	}
	if req.Address != "" {
		query += ` and address = '` + req.Address + `'`
	}
	if req.Role != "" {
		query += ` and role = '` + req.Role + `'`
	}
	if req.AscDesc != "" {
		query += `order by nickname ` + req.AscDesc
	} else {
		query += ` order by nickname asc `
	}

	rows, err := configs.DB.Query(query)
	if err != nil {
		return result, err
	}
	defer rows.Close()
	for rows.Next() {
		var val models.RespGetUsers
		err = rows.Scan(
			&val.Id,
			&val.NickName,
			&val.Age,
			&val.Address,
			&val.Role,
			&val.Password,
			&val.Username,
		)
		val.Password = "######"
		if err != nil {
			return result, err
		}
		result = append(result, val)
	}
	// if len(result) == 0 {
	// 	return result, errors.New("Not Found")
	// }
	return result, nil
}
func GetUser(req models.ReqGetUsers) (result models.RespGetUsers, err error) {
	query := `select id,` + tb + ` from users where true `
	if req.Id != 0 {
		query += ` and id = '` + strconv.Itoa(req.Id) + `'`
	}
	if req.Age != 0 {
		query += ` and age = '` + strconv.Itoa(req.Age) + `'`
	}
	if req.NickName != "" {
		query += ` and user_nickname = '` + req.NickName + `'`
	}
	if req.Username != "" {
		query += ` and username = '` + req.Username + `'`
	}
	if req.Address != "" {
		query += ` and address = '` + req.Address + `'`
	}
	if req.Role != "" {
		query += ` and role = '` + req.Role + `'`
	}
	err = configs.DB.QueryRow(query).Scan(
		&result.Id,
		&result.NickName,
		&result.Age,
		&result.Address,
		&result.Role,
		&result.Password,
		&result.Username,
	)
	if err != nil {
		return result, err
	}
	return result, err
}
func AddUser(req models.ReqGetUsers) (result models.RespGetUsers, err error) {
	query := `insert into users (` + tb + `) values ($1,$2,$3,$4,$5,$6) returning id,` + tb
	err = configs.DB.QueryRow(query, req.NickName, req.Age, req.Address, req.Role, req.Password, req.Username).Scan(
		&result.Id,
		&result.NickName,
		&result.Age,
		&result.Address,
		&result.Role,
		&result.Password,
		&result.Username,
	)
	if err != nil {
		return result, err
	}
	return result, err
}
func DropUser(id int) (status bool) {
	query := `delete from users where id=$1`
	_, err := configs.DB.Exec(query, id)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
func UpdateUser(req models.ReqGetUsers) (result models.RespGetUsers, err error) {
	query := `update users set 
	nickname=$1,
	age=$2,
	address=$3,
	role=$4,
	password=$5,
	username=$6
	 where id=$7 returning id,` + tb
	err = configs.DB.QueryRow(query, req.NickName, req.Age, req.Address, req.Role, req.Password, req.Username, req.Id).Scan(
		&result.Id,
		&result.NickName,
		&result.Age,
		&result.Address,
		&result.Role,
		&result.Password,
		&result.Username,
	)
	if err != nil {
		return result, err
	}
	return result, nil
}
