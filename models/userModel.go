package models

type (
	ReqGetUsers struct {
		Id       int    `json:"id"`
		NickName string `json:"nickname"`
		Address  string `json:"address"`
		Age      int    `json:"age"`
		Role     string `json:"role"`
		Password string `json:"password"`
		Username string `json:"username"`
		AscDesc  string `json:"ascDesc"`
	}
	RespGetUsers struct {
		Id       int    `json:"id"`
		NickName string `json:"nickname"`
		Address  string `json:"address"`
		Age      int    `json:"age"`
		Role     string `json:"role"`
		Password string `json:"password"`
		Username string `json:"username"`
	}

	ReqGetDatas struct {
		Id           int    `json:"id"`
		UserId       int    `json:"userId"`
		NickName     string `json:"nickname"`
		PositionId   int    `json:"positionId"`
		PositionName string `json:"positionName"`
		AscDesc      string `json:"ascDesc"`
	}
	RespGetDatas struct {
		Id           int    `json:"id"`
		UserId       int    `json:"userId"`
		UserNickName string `json:"nickname"`
		PositionId   int    `json:"positionId"`
		PositionName string `json:"positionName"`
	}
)
